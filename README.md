# NanoPB per SmartISS

## Protocol Buffer file

All'interno del `metrics.proto` si trova la definizione del messaggio che sarà
usato per comunicare fra IoT gateway e server MQTT. Il messaggio è composto da
un array `metrics` che contiene le metriche.

A sua volta ogni `metric` ha:

- `name`: nome della metrica
- `timestamp`: tempo di lettura del dato
- `timestamp_format`: formato del timestamp, `unix`, `unix_ms`, etc.
- `fields`: un array di campi, dove ogni campo ha
  + `name`: nome del campo
  + `value`: valore del campo
- `tags`: un array di etichette, dove ogni etichetta ha
  + `name`: nome del etichetta
  + `value`: valore del etichetta

Nel repository si trovano già i file `src/metrics.pb.h` e `src/metrics.pb.c`
compilati dal `metrics.proto`. Per ricompilare `src/metrics.pb.h` e
`src/metrics.pb.c` dal `metrics.proto` usare: 

``` shell
make metrics
```

## NanoPB IMPORTANTE!

NanoPB essendo una libreria per i microcontrollori gestisce i campi
dinamici, nel nostro caso array e stringhe, in due modi:

1. Per ogni campo dinamico vengono aggiunti i puntatori alle funzioni `callback`
   per encoding e decoding dei campi. Queste funzioni devono essere scritte.
2. E' possibile definire la dimensione massima per ogni campo dinamico. In
   questo caso nanopb stanzia la memoria necessaria per tenere il numero massimo
   di elementi o la stringa più lunga.
   
Nel nostro caso sia le stringhe che gli array hanno le dimensioni massime
definite per evitare di scrivere tutti i callback per l'esempio. Questi valori
si trovanno nel file `metrics.options` e sono relativi soltanto a nanopb. Se la
memoria stanziata per le stringhe ed array non è abbastanza o supera la memoria
del MCU, si possono cambiare nelle `metrics.options` oppure usare le funzioni
callback.

Array, essendo già inizializati, hanno tutti i campi impostati a zero del
proprio tipo. Quindi se si vogliono codificare questi campi, prima di chiamare
`pb_encode` bisogna scrivere all'interno della struttura quanti campi sono stati
effettivamente scritti usando, per esempio, `metrics.metrics_count = 3` per dire
che sono state aggiunte tre metriche alla struttura `metrics`. 

## Esempio

Nella cartella `src` si trova l'esempio che può essere compilato e lanciato così:

``` shell
make
./bin/main
```

Questo esempio ogni volta andrà a scrivere le stesse tre metriche e le scrive
nel file binario `test.bin`. Questo file può essere usato per controllare la
correttezza di altri programmi che devono decodificare o codificare lo stesso
messaggio.

``` shell
$ hexdump test.bin
0000000 7b0a 0b0a 656d 7274 6369 6e20 6d61 1065
0000010 1a64 7507 696e 5f78 736d 1622 0b0a 6574
0000020 706d 7265 7461 7275 1165 999a 9999 9999
0000030 3fb9 1322 080a 7270 7365 7573 6572 9a11
0000040 9999 9999 f199 223f 0a13 6808 6d75 6469
0000050 7469 1179 cccd cccc cccc 4000 122a 070a
0000060 6166 7463 726f 1279 6607 7269 6e65 657a
0000070 0b2a 060a 6573 6972 6c61 0112 0a31 0a7b
0000080 6d0b 7465 6972 2063 616e 656d 6410 071a
0000090 6e75 7869 6d5f 2273 0a16 740b 6d65 6570
00000a0 6172 7574 6572 9a11 9999 9999 b999 223f
00000b0 0a13 7008 6572 7373 7275 1165 999a 9999
00000c0 9999 3ff1 1322 080a 7568 696d 6964 7974
00000d0 cd11 cccc cccc 00cc 2a40 0a12 6607 6361
00000e0 6f74 7972 0712 6966 6572 7a6e 2a65 0a0b
00000f0 7306 7265 6169 126c 3101 7b0a 0b0a 656d
0000100 7274 6369 6e20 6d61 1065 1a64 7507 696e
0000110 5f78 736d 1622 0b0a 6574 706d 7265 7461
0000120 7275 1165 999a 9999 9999 3fb9 1322 080a
0000130 7270 7365 7573 6572 9a11 9999 9999 f199
0000140 223f 0a13 6808 6d75 6469 7469 1179 cccd
0000150 cccc cccc 4000 122a 070a 6166 7463 726f
0000160 1279 6607 7269 6e65 657a 0b2a 060a 6573
0000170 6972 6c61 0112 0031
0000177
```


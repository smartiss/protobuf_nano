#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "pb_encode.h"
#include "pb_decode.h"
#include "metrics.pb.h"


/*
  Genera una metrica con nome metric_name e al tempo timestamp.
  metric_name = "metric name"
  timestamp = 100
  timestamp_format = "unix_ms"

  La metrica ha come campi (fields):
  - temperature: 0.1
  - pressure: 1.1
  - humidity: 2.1

  La metrics ha come tags:
  - factory: firenze
  - serial: 1
 */
Metric generate_metric(char* metric_name, long long timestamp) {
  Metric metric = Metric_init_default;

  // copy the name
  strcpy(metric.name, metric_name);
  // timestamp in unix epoch milliseconds
  metric.timestamp = timestamp;
  // timestamp_format so that the other end can decode it
  char timestamp_format[128] = "unix_ms";
  strcpy(metric.timestamp_format, timestamp_format);

  // FIELDS
  // 3 random fields are created
  char field_names[3][128] = {"temperature", "pressure", "humidity"};

  int i;
  for (i=0; i < 3; i++) {
    // value is the reading
    metric.fields[i].value = i + 0.1;
    // field name is the name of the reading
    strcpy(metric.fields[i].name, field_names[i]);
  }
  // IMPORTANT
  // set how many fields have been written otherwise they won't be encoded!
  metric.fields_count = 3;

  // TAGS
  // 2 random tags
  char tag_names[2][128] = {"factory", "serial"};
  char tag_values[2][128] = {"firenze", "1"};

  int j;
  for (j=0; j < 2; j++) {
    // value is the reading
    strcpy(metric.tags[j].value, tag_values[j]);
    // field name is the name of the reading
    strcpy(metric.tags[j].name, tag_names[j]);
  }
  // IMPORTANT
  // set how many fields have been written otherwise they won't be encoded!
  metric.tags_count = 2;

  return metric;
};

int main() {
  // buffer for the message
  uint8_t buffer[1028];
  // stream that writes to buffer
  pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));

  // create 3 metrics
  char name[128] = "metric name";
  long long timestamp = 100;
  Metrics metrics = Metrics_init_zero;

  int i;
  for (i=0; i < 3; i++) {
    metrics.metrics[i] = generate_metric(name, timestamp);
  }
  metrics.metrics_count = 3;

  bool status;
  // this will encode the metric to the buffer stream
  status = pb_encode(&stream, Metrics_fields, &metrics);
  /* Then just check for any errors.. */
  if (!status)
    {
      printf("Encoding failed: %s\n", PB_GET_ERROR(&stream));
      return 1;
    }


  // write the buffer to the file for debugging
  // NB: buffer is fixed size, but we get how big the metric is from
  // stream.bytes_written
  FILE *write_bin;
  write_bin = fopen("test.bin","wb");  // w for write, b for binary
  fwrite(buffer,stream.bytes_written,1,write_bin); // write 10 bytes from our buffer
  fclose(write_bin);
}

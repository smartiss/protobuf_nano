# Include the nanopb provided Makefile rules
#

include nanopb.mk

SRC_DIR = src
NANOPB_DIR := src

# Compiler flags to enable all warnings & debug info
CFLAGS = -Wall -g -O0
CFLAGS += -I$(NANOPB_DIR)

# C source code files that are required
CSRC  = $(SRC_DIR)/main.c                   # The main program
CSRC += $(SRC_DIR)/metrics.pb.c             # The compiled protocol definition
CSRC += $(NANOPB_DIR)/pb_encode.c  # The nanopb encoder
CSRC += $(NANOPB_DIR)/pb_decode.c  # The nanopb decoder
CSRC += $(NANOPB_DIR)/pb_common.c  # The nanopb common parts

# Build rule for the main program
main: $(CSRC)
	$(CC) $(CFLAGS) -o bin/main $(CSRC)

# Build rule for the protocol
metrics: metrics.proto
	$(PROTOC) $(PROTOC_OPTS) --nanopb_out=src/ metrics.proto
